window.addEventListener("DOMContentLoaded", async () => {
    const url = "http://localhost:8000/api/conferences/";

    function createCard(name, description, pictureUrl, start, end) {
        return `
          <div class="card shadow mt-3">
            <img alt="location" src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
              <h5 class="card-title">${name}</h5>
              <p class="card-text">${description}</p>
            </div>
            <div class="card-footer text-muted">${start} - ${end}</div>
          </div>
        `;
    }
    try {
        const response = await fetch(url);

        if (!response.ok) {
            // Figure out what to do when the response is bad
        } else {
            const data = await response.json();
            let column = 1;
            for (let conference of data.conferences) {
                if (column > 3) {
                    column = 1;
                }
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const start = new Date(
                        details.conference.starts
                    ).toLocaleDateString();
                    const end = new Date(
                        details.conference.ends
                    ).toLocaleDateString();
                    const html = createCard(
                        name,
                        description,
                        pictureUrl,
                        start,
                        end
                    );
                    const col = document.getElementById(`col${column}`);
                    col.innerHTML += html;
                    column += 1;
                }
            }
        }
    } catch (e) {
        console.log(e);
    }
});
